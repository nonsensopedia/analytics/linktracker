import networkx as nx
import model


def make_graph(pages_dict, links_list):
    g = nx.DiGraph()
    g.add_nodes_from(pages_dict.values())

    for link in links_list:
        g.add_edge(link.from_page, link.to_page, link=link)

    return g


def remove_redirects(graph):
    g1 = graph.copy()
    to_delete = []
    for v in g1.nodes:
        if not v.disambig and not v.redirect:
            continue

        for in_link in g1.in_edges(v):
            for out_link in g1.out_edges(v):
                if g1.has_edge(in_link[0], out_link[1]):
                    continue

                link = model.Link(in_link[0], out_link[1], None)
                g1.add_edge(in_link[0], out_link[1], link=link)

        to_delete.append(v)

    g1.remove_nodes_from(to_delete)
    return g1
