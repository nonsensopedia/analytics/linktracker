import pandas as pd
import swifter
import csv
import model


def read_links(timestamp, content_ns=None):
    if content_ns is None:
        content_ns = [0, 100, 102, 104, 106, 108, 114]

    pages = pd.read_csv(f"data/pages_{timestamp}.csv.gz", sep='\t', quoting=csv.QUOTE_NONE,
                        header=0, names=('id', 'ns', 'title', 'redirect', 'disambig'))
    links = pd.read_csv(f"data/links_{timestamp}.csv.gz", sep='\t', quoting=csv.QUOTE_NONE,
                        header=0, names=('from', 'to', 'through'))

    pages['disambig'] = pages['disambig'].apply(lambda x: x != '\\N')
    pages['redirect'] = pages['redirect'].astype('bool')
    pages = pages[pages.ns.isin(content_ns)]

    links = links[links.through == 0]

    pages_dict = {p.id: p for p in pages.swifter.apply(model.Page.from_csv_row, axis=1)}
    links_list = links.swifter.apply(model.Link.from_csv_row, axis=1, pages_dict=pages_dict).dropna()

    return pages_dict, links_list
