import functools


@functools.total_ordering
class Page:
    @classmethod
    def from_csv_row(cls, row):
        return cls(row[0], row[1], row[2], row[3], row[4])

    def __init__(self, id, ns, title, redirect, disambig):
        self.id = id
        self.ns = ns
        self.title = title
        self.redirect = redirect
        self.disambig = disambig

    def __repr__(self):
        return str(self.id)

    def __lt__(self, other):
        return self.id < other.id

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return self.id
