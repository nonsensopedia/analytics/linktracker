class Link:
    def __init__(self, from_page, to_page, through_page):
        self.from_page = from_page
        self.to_page = to_page
        self.through_page = through_page

    @classmethod
    def from_csv_row(cls, row, pages_dict):
        through_page = pages_dict.get(row[2], None) if row[2] != 0 else None
        from_page = pages_dict.get(row[0], None)
        to_page = pages_dict.get(row[1], None)

        if not from_page or not to_page:
            return None

        return cls(pages_dict[row[0]], pages_dict[row[1]], through_page)
